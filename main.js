const figure = document.querySelector('#figure'),
    valueFromSelect = document.querySelector('#select'),
    colorFromInput = document.querySelector('#color');

valueFromSelect.addEventListener('change', () => {
    figure.classList.add(valueFromSelect.value);
});

colorFromInput.addEventListener('input', () => {
    figure.style.backgroundColor = colorFromInput.value;
});